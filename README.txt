                      D R U P A L   TEMPLATE
********************************************************************
Name: Green Worm
Author: Dariusz Koryto <dariusz at koryto dot eu>
Drupal: 6
********************************************************************

DESCRIPTION:
Clear and transparent template in green. 2 columns, 3 footer columns.

Compatible Browsers: Firefox 3, Firefox 4, Opera, Chrome

********************************************************************
INSTALLATION:

1. Download Green Worm

2. Unpack the downloaded file, take the entire green_worm folder
   and place it in your Drupal installation under the following
   locations: sites/all/themes

3. Log in as an administrator on your Drupal site and go to
   Administer > Site building > Themes (admin/build/themes) and
   make Green Worm the default theme.

********************************************************************
