<?php
/**
 * @file
 * Template settings.
 */

/**
 * Override or insert variables into the node templates.
 *
 * @param array $vars
 *   An array of variables to pass to the theme template
 *
 * @return array
 *   Set author information line separately from the full $submitted variable.
 */
function green_worm_preprocess_node(&$vars) {
  // Set author information line separately from the full $submitted variable.
  $vars['authored'] = t('Submitted by') . ' ' . $vars['name'];
  return $vars;
}
