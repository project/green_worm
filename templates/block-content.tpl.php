<?php
/**
 * @file
 * Theme implementation to display a block.
 */

?><?php if (!empty($block->subject)): ?>
    <h2><?php print $block->subject ?></h2>
<?php endif;?>
<p><?php print $block->content ?></p>
