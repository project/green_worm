<?php
/**
 * @file
 * Theme implementation to display a block.
 */
?><div class="gadget">
<?php if ($block->subject): ?>
    <h2><?php print $block->subject ?></h2>
    <div class="clr"></div>
    <?php endif; ?>
    <?php print $block->content ?>
</div>
