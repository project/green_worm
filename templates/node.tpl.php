<?php
/**
 * @file
 * Theme implementation to display a node.
 */
?><div class="article">
    <?php if ($page == 0): ?>
    <h2><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h2>
        <div class="clr"></div>
    <?php endif; ?>
    <?php if ($submitted): ?>
    <p class="infopost"><?php print $authored; ?> on <?php print $date; ?></p>
    <?php endif; ?>
    <p><?php print $content; ?></p>
    <?php if ($page != 0): ?>
    <?php if ($taxonomy): ?>
        <?php print $terms; ?><?php endif;?>
    <?php endif; ?>
    <?php if ($links): ?>
        <?php print $links; ?>
    <?php endif; ?>
</div>
