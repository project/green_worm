<?php
/**
 * @file
 * Theme implementation to display a page.
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
</head>
<body>
<div class="main">
    <div class="header">
          <div class="header_inner">
            <div class="logo">
              <a href="<?php print $base_path ?>"><img src="<?php print $logo ?>" alt="<?php print t('Logo') ?>" title="<?php print t('Home') ?>"/></a>
            </div>
            <div class="logo_name">
              <a href="<?php print $base_path ?>"><?php print $site_name; ?>
                <div class="logo_slogan"><?php print $site_slogan; ?></a>
            </div>
          </div>
          </div>
        <div class="menu_nav">
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        </div></div>
        <div class="clr"></div>
    </div>
    <div class="content">
        <div class="breadcrumb">
            <?php if ($breadcrumb) : print $breadcrumb; endif; ?>
        </div>
        <div class="content_resize">
            <div class="mainbar">
                <?php if ($title): ?>
                <h2 class="title"><?php print $title; ?></h2>
                <?php endif; ?>
                <?php print $help; ?>
                <?php if ($show_messages) :print $messages; endif; ?>
                <?php if ($tabs) : print $tabs; endif; ?>
                <?php print $content; ?>
            </div>
            <?php if ($right): ?>
            <div class="sidebar">
                <?php print $right; ?>
            </div>
            <?php endif; ?>
            <div class="clr"></div>
        </div>
    </div>
    <div class="fr_bottom"><?php if($bottom_1 || $bottom_2 || $bottom_3) : ?>
        <div class="fr_bottom_resize">
            <?php if($bottom_1) : ?>
            <div class="col c1">
                <?php print $bottom_1; ?>
            </div>
            <?php endif; ?>
            <?php if($bottom_2) : ?>
            <div class="col c2">
                <?php print $bottom_2; ?>
            </div>
            <?php endif; ?>
            <?php if($bottom_3) : ?>
            <div class="col c3">
                <?php print $bottom_3; ?>
            </div>
            <?php endif; ?>
            <div class="clr"></div>
        </div>
        <?php endif; ?>
    </div>
    <div class="footer">
        <div class="footer_resize">
            <p class="lf"><?php if ($footer_message) : print $footer_message; endif; ?><span><a href="http://koryto.eu/" title="Dariusz Koryto">Dariusz Koryto</a></span></p>
            <div class="clr"></div>
        </div>
    </div>
<?php print $closure; ?>
</body></html>
